<?php

/**
 * Imports Ubercart 'product' nodes and creates a product display nodes with
 * the attached corresponding product variations.
 */
class ExampleMigrateProductDisplayMigration extends Migration {
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    $this->description = t('Migrate product displays.');

    // Setup source.
    $query = db_select('staging_product_display', 'staging')
              ->fields('staging')
              ->condition('type', $arguments['product_type']);
    $this->source = new MigrateSourceSQL($query);

    // Setup destination.
    $this->destination = new MigrateDestinationNode($arguments['product_type'], array('text_format' => 'full_html'));

    // Setup mapping.
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'staging_id' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'description' => 'Staging table ID',
        ),
      ),
      MigrateDestinationNode::getKeySchema()
    );

    $this->addFieldMapping('title', 'title');
    $this->addFieldMapping('uid', 'uid');
    $this->addFieldMapping('created', 'created');
    $this->addFieldMapping('changed', 'changed');
    $this->addFieldMapping('status', 'status');
    $this->addFieldMapping('field_highlights', 'field_highlights');
    $this->addFieldMapping('field_product', 'field_product');
  }

  public function prepareRow($row) {
    if (parent::prepareRow($current_row) === FALSE) {
      return FALSE;
    }

    $products = array();
    foreach(explode(',', $row->field_product) as $product_id) {
      $products[] = example_product_destination_id($product_id);
    }
    $row->field_product = $products;
  }
}

/**
 * Retrieves the corresponding Commerce Product ID of a migrated product.
 */
function example_product_destination_id($source_id) {
  // Get the corresponding Migrate API mapping table.
  $map_table = 'migrate_map_example_product';
  return db_query('SELECT destid1 FROM {'.$map_table.'} WHERE sourceid1 = :source_id', array(
    ':source_id' => $source_id))->fetchField();
}
