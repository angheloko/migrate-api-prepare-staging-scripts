<?php

/**
 * Initializes the product's staging table.
 */
function example_init_staging_product() {
  // Get a connection to the source database.
  $connection = example_migrate_get_source_connection();

  // Get all products that will be transformed to product variations.
  $query = $connection->select('node', 'node');
  $query->innerJoin('uc_products', 'uc_products',
    'node.nid = uc_products.nid AND node.vid = uc_products.vid');

  $query->fields('node', array('nid', 'vid', 'type', 'title', 'created', 'changed', 'uid'));
  $query->fields('uc_products', array('list_price', 'cost', 'sell_price'));

  $query->condition('node.type', 'product');
  $query->orderBy('node.changed', 'ASC');

  $result = $query->execute();
  foreach ($result as $record) {
    $product_variations = array();

    $values = array(
      'nid' => $record->nid,
      'uid' => $record->uid,
      'created' => $record->created,
      'changed' => $record->changed,
      'sell_price' => $record->sell_price,
    );

    // Get the Ubercart options associated to this node.
    $product_options = example_migrate_get_product_attributes($record->nid);
    if (count($product_options) > 1) {
      // We know that we only have a maximum of 2 attributes...
      $product_options1 = array_shift($product_options);
      $product_options2 = array_shift($product_options);

      // Get all possible permutations of the 2 attributes.
      foreach ($product_options1 as $opt1) {
        // Determine the new attribute field to use for the first option.
        // This will be mapped later to a field in the Commerce Product entity.
        $field_name1 = $opt1['attribute_name'];
        $values[$field_name1] = $opt1['option_name'];

        foreach ($product_options2 as $opt2) {
          $variation = array();

          $combination = array();
          $combination[$opt1['aid']] = $opt1['oid'];
          $combination[$opt2['aid']] = $opt2['oid'];

          $variation['title'] = $record->title;

          $variation['oid1'] = $opt1['oid'];
          $variation['oid2'] = $opt2['oid'];

          // Determine the new attribute field to use for the second option.
          // This will be mapped later to a field in the Commerce Product entity.
          $field_name2 = $opt2['attribute_name'];
          $values[$field_name2] = $opt2['option_name'];

          $product_variations[] = $values + $variation;
        }
      }

      // Insert/update the derived product variations.
      foreach ($product_variations as $variation) {
        // Insert the record if not yet existing, else, update it.
        $exists_query = db_select('example_migrate_staging_product');
        $exists_query->addExpression('1');
        $exists_query->condition('nid', $variation['nid']);

        // Use the nid and options as conditions in determining if a product
        // variation already exists.
        if (isset($variation['oid1'])) {
          $exists_query->condition('oid1', $variation['oid1']);
        }
        if (isset($variation['oid2'])) {
          $exists_query->condition('oid2', $variation['oid2']);
        }

        $exists = $exists_query->execute()->fetchField();
        if ($exists) {
          // We have an existing product variation...
          $import_query = db_update('example_migrate_staging_product')
            ->fields($variation);

          $import_query->condition('nid', $variation['nid']);
          if (isset($variation['oid1'])) {
            $import_query->condition('oid1', $variation['oid1']);
          }
          if (isset($variation['oid2'])) {
            $import_query->condition('oid2', $variation['oid2']);
          }

          $import_query->execute();
          $updated_count++;
        }
        else {
          // We have a new product variation...
          $import_query = db_insert('example_migrate_staging_product')
            ->fields($variation);

          $import_query->execute();
          $inserted_count++;
        }
      } // foreach ($product_variations as $variation)
    } // if (count($product_options) > 1)
  } // foreach ($result as $record)

  // Create the product displays.
  $result = db_query('SELECT DISTINCT nid, type FROM {example_migrate_staging_product}');
  if ($result) {
    while ($record = $result->fetchObject()) {
      example_migrate_create_product_display($record->nid);
    }
  }
}

/**
 * Creates the corresponding product display for staged products.
 */
function example_migrate_create_product_display($nid) {
  db_query("DELETE FROM {example_migrate_staging_product_display} WHERE nid = :nid", array(':nid' => $nid));

  $connection = example_migrate_get_source_connection();
  $query = $connection->select('node', 'n');

  $query->innerJoin('uc_products', 'up',
    'n.nid = up.nid AND n.vid = up.vid');
  $query->leftJoin('content_field_highlights', 'h',
    'n.nid = h.nid AND n.vid = h.vid');

  $query->fields('n', array('nid', 'type', 'title', 'created', 'changed', 'uid', 'status'));
  $query->fields('h', array('field_highlights_value'));

  $query->condition('n.nid', $nid);

  if ($record = $query->execute()->fetchObject()) {
    $values = array(
      'nid' => $record->nid,
      'type' => $type,
      'title' => $record->title,
      'uid' => $record->uid,
      'status' => $record->status,
      'created' => $record->created,
      'changed' => $record->changed,
    );

    // Highlights.
    $values['field_highlights'] = $record->field_highlights_value;

    // Product variations.
    $product_variations = example_migrate_get_product_variations($nid);
    if (!empty($product_variations)) {
      $values['field_product'] = implode(',', $product_variations);
    }

    db_insert('example_migrate_staging_product_display')
     ->fields($values)
     ->execute();
  }
}

/**
 * Returns an array of options.
 */
function example_migrate_get_product_attributes($nid) {
  $options = array();
  $connection = example_migrate_get_source_connection();

  $query = $connection->select('node', 'node');
  $query->leftJoin('uc_product_options', 'uc_product_options',
    'node.nid = uc_product_options.nid');
  $query->leftJoin('uc_attribute_options', 'uc_attribute_options',
    'uc_product_options.oid = uc_attribute_options.oid');
  $query->leftJoin('uc_attributes', 'uc_attributes',
    'uc_attribute_options.aid = uc_attributes.aid');
  $query->leftJoin('uc_product_attributes', 'uc_product_attributes',
    'uc_attributes.aid = uc_product_attributes.aid AND node.nid = uc_product_attributes.nid');

  $query->fields('node', array('nid', 'vid', 'type', 'title', 'created', 'changed', 'uid'));
  $query->fields('uc_product_options', array('oid'));
  $query->fields('uc_attribute_options', array('name', 'aid'));
  $query->fields('uc_attributes', array('name'));
  $query->fields('uc_product_attributes', array('label'));

  $query->condition('node.nid', $nid);
  $query->orderBy('uc_product_attributes.ordering', 'ASC')
        ->orderBy('uc_product_options.weight', 'ASC');

  $result = $query->execute();
  foreach ($result as $record) {
    $aid = $record->aid ? $record->aid : 0;
    $oid = $record->oid ? $record->oid : 0;

    $options[$aid][$oid] = array(
      'aid' => $aid,
      'oid' => $oid,
      'attribute_name' => !empty($record->label) ? $record->label : $record->uc_attributes_name,
      'option_name' => $record->name,
    );
  }

  return $options;
}

/**
 * Returns an array of staging IDs of imported product variations.
 */
function example_migrate_get_product_variations($nid) {
  $products = array();

  $query = db_select('example_migrate_staging_product', 'staging');
  $query->fields('staging', array('staging_id'))
    ->condition('staging.nid', $nid);

  $result = $query->execute();
  foreach ($result as $record) {
    $products[] = $record->staging_id;
  }

  return $products;
}

/**
 * Gets DB connection to legacy (source) DB.
 */
function example_migrate_get_source_connection() {
  $migration_source_db = variable_get('example_migrate_source_database', 'legacy');
  if (empty($GLOBALS['databases'][$migration_source_db]['default'])) {
    $migration_source_db = NULL;
  }
  $connection = Database::getConnection('default', $migration_source_db);
  return $connection;
}
