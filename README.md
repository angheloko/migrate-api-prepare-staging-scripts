Migration staging preparation scripts for D6/Ubercart - D7/Commerce
==========================================================

What is this?
-----------

This collection of scripts provide an example on how to tackle migration of a
D6/Ubercart setup to a D7/Commerce setup.

These scripts are meant to give you an idea on how to approach the migration
process, which is, basically, by introducing the "staging" process. The
"staging" process aims to remove all the heavy lifting away from your
Migrate API Migration classes thus keeping your Migrate API files cleaner,
easier to understand, and, hopefully, more efficient.

The scripts
----------

* **migration-prepare-staging.php**
    This script gives you an example on how exactly to create your "staging"
    tables. All of the heavy lifting will be accomplished here, such as the
    transformation of product attributes to product variations making them
    "Commerce-ready".

* **migration-product.php**
    This script gives you an example or template of a Migrate API migration class
    for your Commerce Product entities. The class uses the product staging table
    created by the migration-prepare-staging.php script. Since the records in the
    product staging table has been prepared to be "Commerce-ready", the resulting
    codes are much more straightforward.

* **migration-product-display.php**
    This script, like the migration-product.php script, gives you an example or
    template of a Migrate API migration class for your Product Display entities.
    The class uses the product display staging table created by the
    migration-prepare-staging.php script. Similarly, since the records in the
    product display staging table has been prepared to be "Commerce-ready", the
    resulting codes are much more straightforward.

