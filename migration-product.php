<?php

/**
 * Imports and transforms records from example_migrate_staging_product table into
 * Commerce Product objects.
 */
class ExampleMigrateProductMigration extends Migration {
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    $this->description = t('Migrate products.');

    // Setup source.
    $query = db_select('example_migrate_staging_product', 'staging')
              ->fields('staging');
    $this->source = new MigrateSourceSQL($query);

    // Setup destination.
    $this->destination = new MigrateDestinationEntityAPI('commerce_product', $arguments['product_type']);

    // Setup mapping.
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'staging_id' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'description' => 'Staging table ID',
        ),
      ),
      MigrateDestinationEntityAPI::getKeySchema('commerce_product', $arguments['product_type'])
    );

    $this->addFieldMapping('title', 'title');
    $this->addFieldMapping('type', 'type');
    $this->addFieldMapping('uid', 'uid');
    $this->addFieldMapping('status', 'status');
    $this->addFieldMapping('created', 'created');
    $this->addFieldMapping('changed', 'changed');
    $this->addFieldMapping('commerce_price', 'sell_price');

    // Color
    $this->addFieldMapping('field_color', 'color');
    $this->addFieldMapping('field_color:create_term')
      ->defaultValue(TRUE);

    // Size
    $this->addFieldMapping('field_size', 'size');
    $this->addFieldMapping('field_size:create_term')
      ->defaultValue(TRUE);
  }
}
